package sort

func InsertionSort(arr SortableArray) SortableArray {
	for sortedIdx := 0; sortedIdx < len(arr)-1; sortedIdx++ {
		for i := sortedIdx; i >= 0 && arr[i].Value() > arr[i+1].Value(); i-- {
			arr[i], arr[i+1] = arr[i+1], arr[i]
		}
	}
	return arr
}
