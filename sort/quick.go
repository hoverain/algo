package sort

func QuickSort(arr SortableArray) SortableArray {
	if len(arr) <= 1 {
		return arr
	}
	i, j := 0, len(arr)-1
	pivot := arr[0].Value()
	for {
		for i < j && arr[j].Value() >= pivot {
			j--
		}
		for i < j && arr[i].Value() <= pivot {
			i++
		}
		if i < j {
			arr[i], arr[j] = arr[j], arr[i]
		} else {
			arr[0], arr[i] = arr[i], arr[0]
			break
		}
	}
	QuickSort(arr[:i])
	QuickSort(arr[i+1:])
	return arr
}
