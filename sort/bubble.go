package sort

func BubbleSort(arr SortableArray) SortableArray {
	for swapped, last := true, len(arr)-1; swapped; last-- {
		swapped = false
		for i := 0; i < last; i++ {
			if arr[i].Value() > arr[i+1].Value() {
				arr[i], arr[i+1] = arr[i+1], arr[i]
				swapped = true
			}
		}
	}
	return arr
}
