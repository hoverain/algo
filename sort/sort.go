package sort

type Sortable interface {
	// return index of this item in the original array
	Index() int
	// return value for use in the sort algrithm
	Value() int
}

type SortableArray []Sortable

func (sa SortableArray) Indices() []int {
	indices := make([]int, len(sa))
	for i := range sa {
		indices[i] = sa[i].Index()
	}
	return indices
}

func (sa SortableArray) Values() []int {
	values := make([]int, len(sa))
	for i := range sa {
		values[i] = sa[i].Value()
	}
	return values
}

func (sa *SortableArray) Sort(fn func(SortableArray) SortableArray) {
	*sa = fn(*sa)
}

type sortable struct {
	value int
	index int
}

func (s sortable) Value() int {
	return s.value
}

func (s sortable) Index() int {
	return s.index
}

func NewSortableArray(arr []int) SortableArray {
	s := make(SortableArray, len(arr))
	for i := range arr {
		s[i] = sortable{arr[i], i}
	}
	return s
}
