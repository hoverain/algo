package sort

func HeapSort(arr SortableArray) SortableArray {
	for i := len(arr)/2 - 1; i >= 0; i-- {
		sink(arr, i, len(arr)-1)
	}
	for i := len(arr) - 1; i > 0; {
		arr[0], arr[i] = arr[i], arr[0]
		i--
		sink(arr, 0, i)
	}
	return arr
}

func sink(arr SortableArray, i int, max int) {
	for 2*i+1 <= max {
		next := 2*i + 1
		if next+1 <= max && arr[next+1].Value() > arr[next].Value() {
			next++
		}
		if arr[i].Value() > arr[next].Value() {
			break
		}
		arr[i], arr[next] = arr[next], arr[i]
		i = next
	}
}
