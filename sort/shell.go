package sort

import "fmt"

// default delta list generator, by Knuth
func defaultDeltas(length int) []int {
	result := []int{1}
	delta := 1
	for delta < length/3 {
		delta = delta*3 + 1
		result = append(result, delta)
	}
	return result
}

func GenShellFunc(
	genDelta func(length int) []int,
) func(SortableArray) SortableArray {
	return func(arr SortableArray) SortableArray {
		deltas := genDelta(len(arr))
		if len(deltas) < 1 || deltas[0] != 1 {
			fmt.Println("Warning: invalid genDelta function, degenerating to Insertion-Merge")
			deltas = []int{1}
		}
		for d := len(deltas) - 1; d >= 0; d-- {
			delta := deltas[d]
			fmt.Println(delta)
			for sortedIdx := 0; sortedIdx < len(arr)-delta; sortedIdx++ {
				for i := sortedIdx; i >= 0 && arr[i].Value() > arr[i+delta].Value(); i -= delta {
					arr[i], arr[i+1] = arr[i+1], arr[i]
				}
			}
		}
		return arr
	}
}

var ShellSort = GenShellFunc(defaultDeltas)
