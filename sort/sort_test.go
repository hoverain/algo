package sort_test

import (
	"hover/algo/sort"
	"reflect"
	"testing"
)

type TestData struct {
	src     []int
	values  []int
	indices []int
}

var testGroups = []TestData{
	{
		[]int{},
		[]int{},
		[]int{},
	},
	{
		[]int{1},
		[]int{1},
		[]int{0},
	},
	{
		[]int{4, 2, 7, 3, 1, 4, 5, 4, 6},
		[]int{1, 2, 3, 4, 4, 4, 5, 6, 7},
		[]int{4, 1, 3, 0, 5, 7, 6, 8, 2},
	},
	{
		[]int{4, 2, 7, 3, 4, 4, 5, 1, 6},
		[]int{1, 2, 3, 4, 4, 4, 5, 6, 7},
		[]int{7, 1, 3, 0, 4, 5, 6, 8, 2},
	},
}

func TestBubble(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.BubbleSort)
		values := sarr.Values()
		indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		if !reflect.DeepEqual(indices, testdata.indices) {
			t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		}
	}
}

func TestInsertion(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.InsertionSort)
		values := sarr.Values()
		indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		if !reflect.DeepEqual(indices, testdata.indices) {
			t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		}
	}
}

func TestSelection(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.SelectionSort)
		values := sarr.Values()
		// indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		// if !reflect.DeepEqual(indices, testdata.indices) {
		// 	t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		// }
	}
}

func TestMerge(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.MergeSort)
		values := sarr.Values()
		indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		if !reflect.DeepEqual(indices, testdata.indices) {
			t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		}
	}
}

func TestShell(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.ShellSort)
		values := sarr.Values()
		indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		if !reflect.DeepEqual(indices, testdata.indices) {
			t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		}
	}
}

func TestQuick(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.QuickSort)
		values := sarr.Values()
		// indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		// if !reflect.DeepEqual(indices, testdata.indices) {
		// 	t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		// }
	}
}

func TestHeap(t *testing.T) {
	for _, testdata := range testGroups {
		sarr := sort.NewSortableArray(testdata.src)
		sarr.Sort(sort.HeapSort)
		values := sarr.Values()
		// indices := sarr.Indices()
		t.Log(testdata.src)
		t.Log(sarr)
		if !reflect.DeepEqual(values, testdata.values) {
			t.Errorf("expect values to be : %v\ngot: %v", testdata.values, values)
		}
		// if !reflect.DeepEqual(indices, testdata.indices) {
		// 	t.Errorf("expect indices to be : %v\ngot: %v", testdata.indices, indices)
		// }
	}
}
