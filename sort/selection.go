package sort

func SelectionSort(arr SortableArray) SortableArray {
	last := len(arr) - 1
	for last > 0 {
		maxIndex, maxValue := last, arr[last].Value()
		for i := last - 1; i >= 0; i-- {
			if v := arr[i].Value(); v > maxValue {
				maxIndex, maxValue = i, v
			}
		}
		arr[maxIndex], arr[last] = arr[last], arr[maxIndex]
		last--
	}
	return arr
}
