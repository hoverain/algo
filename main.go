package main

import (
	"fmt"
)

type Person struct {
	name string
	age  int
}

func (p *Person) String() string {
	return fmt.Sprint("haha, ", *p)
}

func main() {
	p := &Person{"tom", 3}
	fmt.Printf("%p, %v\n", p, p)
	p1 := *p
	fmt.Printf("%p, %v\n", &p1, &p1)
}
