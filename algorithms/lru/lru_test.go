package lru_test

import (
	"hover/algo/algorithms/lru"
	"testing"
)

func TestLru(t *testing.T) {
	container := lru.NewContainer(
		2,
		make(lru.MapSource, 3),
		&lru.SimpleArrList{},
	)
	container.Set("a", 1)
	container.Set("b", 2)
	container.Set("c", 3)
	t.Log(container.Get("a"))
	t.Log(container.Get("b"))
	t.Log(container.Get("c"))
}

func TestDebug(t *testing.T) {
	var arr []string
	t.Log(arr)
	arr = append(arr, "1")
	t.Log(arr)
}
