package lru

type Container interface {
	Get(key string) (value interface{}, ok bool)
	Set(key string, value interface{})
}

type List interface {
	MoveToLast(key string)
	PopFirst() string
}

type Source interface {
	Get(key string) (value interface{}, ok bool)
	Set(key string, value interface{})
	Del(key string)
	Size() int
}

type container struct {
	MaxSize int
	Source
	List List
}

func (c container) Get(key string) (value interface{}, ok bool) {
	if value, ok = c.Source.Get(key); ok {
		c.List.MoveToLast(key)
	}
	return
}

func (c container) Set(key string, value interface{}) {
	c.Source.Set(key, value)
	c.List.MoveToLast(key)
	for c.Source.Size() > c.MaxSize {
		evictedKey := c.List.PopFirst()
		c.Source.Del(evictedKey)
	}
}

type MapSource map[string]interface{}

func (m MapSource) Get(key string) (value interface{}, ok bool) {
	value, ok = m[key]
	return
}

func (m MapSource) Set(key string, value interface{}) {
	m[key] = value
}

func (m MapSource) Del(key string) {
	delete(m, key)
}

func (m MapSource) Size() int {
	return len(m)
}

var _ Source = MapSource{}

type SimpleArrList []string

func (l *SimpleArrList) MoveToLast(key string) {
	list := *l
	for i, s := range list {
		if s == key {
			copy(list[i:], list[i+1:])
			list[len(list)-1] = s
			return
		}
	}
	list = append(list, key)
	*l = list
}

func (l *SimpleArrList) PopFirst() string {
	list := *l
	first := list[0]
	*l = list[1:]
	return first
}

var _ List = &SimpleArrList{}

func NewContainer(maxSize int, src Source, list List) Container {
	size := src.Size()
	if size > 0 {
		panic("should use a zero size source")
	}
	return container{
		maxSize, src, list,
	}
}
