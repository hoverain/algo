package lru2

import (
	"fmt"
	"testing"
)

func PrintLru(l *Lru) {
	for e := l.List.Front(); e != nil; e = e.Next() {
		fmt.Println(e)
	}
}

func TestSimple(_ *testing.T) {
	l := New(3)
	l.Set("a", 1)
	l.Set("b", 2)
	l.Set("c", 3)
	l.Set("d", 4)
	fmt.Println(l.Get("a"))
	fmt.Println(l.Get("b"))

	PrintLru(l)
	l.Del("d")
	PrintLru(l)

}
