package lru2

import (
	"container/list"
)

type KV struct {
	K string
	V interface{}
}

type Lru struct {
	Size int
	List *list.List
	Dict map[string]*list.Element
}

func New(size int) *Lru {
	return &Lru{
		Size: size,
		List: list.New(),
		Dict: make(map[string]*list.Element, size),
	}
}

func (l *Lru) Get(key string) interface{} {
	e, ok := l.Dict[key]
	if !ok {
		return nil
	}
	l.List.MoveToFront(e)
	return e.Value.(KV).V
}

func (l *Lru) Set(key string, value interface{}) {
	e, ok := l.Dict[key]
	if ok {
		l.List.Remove(e)
	}
	if l.List.Len() >= l.Size {
		delete(l.Dict, l.List.Back().Value.(KV).K)
		l.List.Remove(l.List.Back())
	}
	l.List.PushFront(KV{key, value})
	l.Dict[key] = l.List.Front()
}

func (l *Lru) Del(key string) interface{} {
	e, ok := l.Dict[key]
	if !ok {
		return nil
	} else {
		delete(l.Dict, key)
		l.List.Remove(e)
	}
	return e.Value.(KV).V
}
