package lru2

// type Node struct {
// 	prev, next *Node
// 	list       *List
// 	Value      interface{}
// }

// func (n *Node) Prev() *Node {
// 	return n.prev
// }

// func (n *Node) Next() *Node {
// 	return n.next
// }

// type List struct {
// 	size int
// 	ref  *Node
// }

// func New() *List {
// 	list := &List{
// 		0,
// 		&Node{
// 			prev:  nil,
// 			next:  nil,
// 			list:  nil,
// 			Value: nil,
// 		},
// 	}
// 	list.ref.list = list
// 	return list
// }

// func (l *List) Back() *Node {
// 	return l.ref.prev
// }

// func (l *List) Front() *Node {
// 	return l.ref.next
// }

// // func (l *List) Init() *List {

// // }

// func (l *List) InsertAfter(v interface{}, mark *Node) *Node {
// 	newNode := &Node{
// 		prev:  mark,
// 		next:  mark.next,
// 		list:  l,
// 		Value: v,
// 	}
// 	mark.next.prev = newNode
// 	mark.next = newNode
// 	l.size++
// 	return newNode
// }

// func (l *List) InsertBefore(v interface{}, mark *Node) *Node {
// 	newNode := &Node{
// 		prev:  mark.prev,
// 		next:  mark,
// 		list:  l,
// 		Value: v,
// 	}
// 	mark.prev.next = newNode
// 	mark.prev = newNode
// 	l.size++
// 	return newNode
// }

// func (l *List) Len() int {
// 	return l.size
// }

// func (l *List) MoveAfter(e, mark *Node) {
// 	e.prev.next = e.next
// 	e.next.prev = e.prev
// 	e.prev = mark
// 	e.next = mark.next
// 	mark.next.prev = e
// 	mark.next = e
// }

// func (l *List) MoveBefore(e, mark *Node) {
// 	e.prev.next = e.next
// 	e.next.prev = e.prev
// 	e.prev = mark.prev
// 	e.next = mark
// 	mark.prev.next = e
// 	mark.prev = e
// }

// func (l *List) MoveToBack(e *Node) {
// 	l.MoveAfter(e, l.Back())
// }

// func (l *List) MoveToFront(e *Node) {
// 	l.MoveBefore(e, l.Front())
// }

// func (l *List) PushBack(v interface{}) *Node {
// 	n := &Node{
// 		prev:  l.ref.prev,
// 		next:  l.ref,
// 		list:  l,
// 		Value: v,
// 	}
// 	l.ref.prev.next = nil
// 	l.ref.prev = n
// 	l.size++
// 	return n
// }

// func (l *List) PushBackList(other *List) {
// 	l.size += other.size
// 	l.ref.prev.next = other.ref.next
// 	other.ref.next.prev = l.ref.prev
// 	l.ref.prev = other.ref.prev
// 	other.ref.prev.next = nil
// }

// func (l *List) PushFront(v interface{}) *Node {
// 	n := &Node{
// 		prev:  l.ref,
// 		next:  l.ref.next,
// 		list:  l,
// 		Value: v,
// 	}
// 	l.ref.next.prev = nil
// 	l.ref.next = n
// 	l.size++
// 	return n
// }

// func (l *List) PushFrontList(other *List) {
// 	l.size += other.size
// 	l.ref.next.prev = other.ref.next
// 	other.ref.prev.next = l.ref.next
// 	l.ref.next = other.ref.next
// 	other.ref.next.prev = nil
// }

// func (l *List) Remove(e *Node) interface{} {
// 	e.prev.next = e.next
// 	e.next.prev = e.prev
// 	l.size--
// 	return e.Value
// }
