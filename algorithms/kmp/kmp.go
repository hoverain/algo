package kmp

func next(s string) (indices []int) {
	length := len(s)
	indices = make([]int, length)
	indices[0], indices[1] = 0, 0
	if length < 2 {
		return indices[:length]
	}
	for i := 1; i < length-1; i++ {
		k := indices[i]
		for {
			if s[k] == s[i] {
				indices[i+1] = k + 1
				break
			} else if k == 0 {
				indices[i+1] = 0
				break
			} else {
				k = indices[k]
			}
		}
	}
	return
}

func IndexOf(sub string, src string) int {
	if sub == "" {
		return 0
	}
	if src == "" {
		return -1
	}
	if sub == src {
		return 0
	}
	lensub, lensrc := len(sub), len(src)
	if lensub >= lensrc {
		return -1
	}
	i, j := 0, 0
	indices := next(sub)
	for {
		if j >= lensub {
			return i - lensub
		}
		if i > lensrc-lensub {
			return -1
		}
		if src[i] == sub[j] {
			i++
			j++
		} else if j == 0 {
			i++
		} else {
			j = indices[j]
		}
	}
}
