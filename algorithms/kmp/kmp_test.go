package kmp

import (
	"fmt"
	"math/rand"
	"strings"
	"testing"
)

type Sample struct {
	sub   string
	src   string
	index int
}

var samples []Sample

func init() {
	// rand.Seed(time.Now().UnixNano())
	samples = []Sample{
		{"", "", -1},
		{"", "a", -1},
		{"a", "", -1},
		{"a", "a", -1},
		{"a", "b", -1},
		{"abaabe", "abaabaabecaabaabe", -1},
		{randstr("ab", 4), randstr("ab", 100), -1},
		{randstr("ab", 4), randstr("ab", 100), -1},
		{randstr("abc", 4), randstr("abc", 100), -1},
	}
	fmt.Println("test samples: ")
	for i := range samples {
		samples[i].index = strings.Index(samples[i].src, samples[i].sub)
		fmt.Printf("%#v\n", samples[i])
	}
}

func randstr(set string, length int) string {
	lenset := len(set)
	bytes := make([]byte, length)
	for i := range bytes {
		bytes[i] = set[rand.Intn(lenset)]
	}
	return string(bytes)
}

func TestIndexOf(t *testing.T) {
	for _, sample := range samples {
		if IndexOf(sample.sub, sample.src) != sample.index {
			t.Errorf("fail sample: %#v", sample)
		}
	}
}
