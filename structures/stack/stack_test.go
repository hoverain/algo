package stack_test

import (
	"hover/algo/structures/stack"
	"testing"
)

func TestStack(t *testing.T) {
	s := new(stack.Stack)
	s.Push(1)
	s.Push(2)
	two, ok := s.Pop()
	if two != 2 || !ok {
		t.Fatalf("expect two(%v) ok(%v), got two(%v) ok(%v)", 2, true, two, ok)
	}
	size := s.Size()
	if size != 1 {
		t.Fatalf("expect size(%v), got size(%v)", 1, size)
	}
	top, ok := s.Top()
	if top != 1 || !ok {
		t.Fatalf("expect top(%v) ok(%v), got top(%v) ok(%v)", 1, true, top, ok)
	}
	one, ok := s.Pop()
	if one != 1 || !ok {
		t.Fatalf("expect one(%v) ok(%v), got one(%v) ok(%v)", 1, true, one, ok)
	}
	top, ok = s.Top()
	if top != nil || ok {
		t.Fatalf("expect top(%v) ok(%v), got top(%v) ok(%v)", nil, false, top, ok)
	}
	n, ok := s.Pop()
	if n != nil || ok {
		t.Fatalf("expect n(%v) ok(%v), got n(%v) ok(%v)", nil, false, n, ok)
	}
}
