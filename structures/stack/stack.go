package stack

type Stack []interface{}

func (s *Stack) Size() int {
	return len(*s)
}

func (s *Stack) Push(value interface{}) {
	*s = append(*s, value)
}

func (s *Stack) Pop() (value interface{}, ok bool) {
	l := len(*s)
	if l == 0 {
		return
	}
	*s, value, ok = (*s)[0:l-1], (*s)[l-1], true
	return
}

func (s *Stack) Top() (value interface{}, ok bool) {
	l := len(*s)
	if l == 0 {
		return
	}
	return (*s)[l-1], true
}
