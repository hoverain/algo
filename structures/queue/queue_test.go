package queue_test

import (
	"hover/algo/structures/queue"
	"testing"
)

func TestQueue(t *testing.T) {
	q := new(queue.Queue)
	q.Push(1)
	q.Push(2)
	v, ok := q.First()
	if v != 1 || !ok {
		t.Fatalf("expect v(%v) ok(%v), got v(%v) ok(%v)", 1, true, v, ok)
	}
	v, ok = q.Pop()
	if v != 1 || !ok {
		t.Fatalf("expect v(%v) ok(%v), got v(%v) ok(%v)", 1, true, v, ok)
	}
	size := q.Size()
	if size != 1 {
		t.Fatalf("expect size(%v), got size(%v)", 1, size)
	}

	v, ok = q.Pop()
	if v != 2 || !ok {
		t.Fatalf("expect v(%v) ok(%v), got v(%v) ok(%v)", 1, true, v, ok)
	}
	n, ok := q.Pop()
	if n != nil || ok {
		t.Fatalf("expect n(%v) ok(%v), got n(%v) ok(%v)", nil, false, n, ok)
	}
}
