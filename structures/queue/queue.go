package queue

type Queue []interface{}

func (q *Queue) Size() int {
	return len(*q)
}

func (q *Queue) Push(value interface{}) {
	*q = append(*q, value)
}

func (q *Queue) Pop() (value interface{}, ok bool) {
	l := len(*q)
	if l == 0 {
		return
	}
	*q, value, ok = (*q)[1:], (*q)[0], true
	return
}

func (q *Queue) First() (value interface{}, ok bool) {
	l := len(*q)
	if l == 0 {
		return
	}
	return (*q)[0], true
}
