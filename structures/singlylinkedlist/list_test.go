package singlylinkedlist_test

import (
	linkedlist "hover/algo/structures/singlylinkedlist"
	"testing"
)

func listShouldBe(t *testing.T, list *linkedlist.List, values []interface{}) {
	t.Log("list and values: ", list, values)
	if list.Len() != len(values) {
		t.Fatalf("expected Len(%v) to be %v\n", list.Len(), len(values))
	}
	for i, value := range values {
		v := list.Value(i)
		if v != value {
			t.Fatalf("expected Value(%v): %v to be %v\n", i, v, value)
		}
	}
}

func TestDebug(t *testing.T) {
	node := linkedlist.NewNode(1, nil)
	t.Logf("node: %+v", node)
	t.Logf("node: %+v", *node)

	list := linkedlist.NewList(1, 2, 3)
	t.Logf("list: %#v", list)
	t.Logf("list: %#v", *list)

	cp := list.Clone()
	t.Logf("list: %#v", cp)
	t.Logf("list: %#v", *cp)
}

func TestCreateAndAppendAndClone(t *testing.T) {
	var valueSamples = [][]interface{}{
		{},
		{1},
		{1, 2},
		{1, 2, 3},
	}
	for _, values := range valueSamples {
		list := linkedlist.NewList(values...)
		listShouldBe(t, list, values)
		for _, newValues := range valueSamples {
			values := append(append([]interface{}{}, values...), newValues...)
			list := list.Clone()
			list.Append(newValues...)
			listShouldBe(t, list, values)
		}
	}
}

func TestInsertAndDeleteAnd(t *testing.T) {
	list := linkedlist.NewList(1, 2, 3)
	list.Insert(0, 4)
	listShouldBe(t, list, []interface{}{4, 1, 2, 3})
	list.Delete(0)
	listShouldBe(t, list, []interface{}{1, 2, 3})
	list.Insert(2, 4)
	listShouldBe(t, list, []interface{}{1, 2, 4, 3})
	list.Delete(2)
	listShouldBe(t, list, []interface{}{1, 2, 3})
}

func TestReverse(t *testing.T) {
	type sample struct {
		src []interface{}
		dst []interface{}
	}
	sps := []sample{
		{
			[]interface{}{},
			[]interface{}{},
		},
		{
			[]interface{}{1},
			[]interface{}{1},
		},
		{
			[]interface{}{1, 2},
			[]interface{}{1, 2},
		},
		{
			[]interface{}{1, 2, 3},
			[]interface{}{3, 2, 1},
		},
	}
	for _, s := range sps {
		list := linkedlist.NewList(s.src...)
		list.Reverse()
		listShouldBe(t, list, s.dst)
	}
}
