package singlylinkedlist

import (
	"errors"
	"fmt"
)

type List struct {
	len  int
	head *Node
	tail *Node
}

var (
	ErrOutOfRange = errors.New("index out of range")
)

func NewList(values ...interface{}) *List {
	list := new(List)
	list.Append(values...)
	return list
}

func (l List) String() string {
	return fmt.Sprint(l.ToArray())
}

func (l *List) Len() int {
	return l.len
}

func (l *List) Head() *Node {
	return l.head
}

func (l *List) Tail() *Node {
	return l.tail
}

func (l *List) ToArray() []*Node {
	var nodes []*Node
	node := l.head
	for node != nil {
		nodes = append(nodes, node)
		node = node.next
	}
	return nodes
}

func (l *List) checkIndex(index int) {
	if index < 0 || index >= l.len {
		panic(ErrOutOfRange)
	}
}

func (l *List) find(index int) *Node {
	l.checkIndex(index)
	node := l.head
	for i := 0; i < index; i++ {
		node = node.next
	}
	return node
}

func (l *List) Append(values ...interface{}) {
	newCount := len(values)
	if newCount == 0 {
		return
	}
	head := &Node{values[0], nil}
	tail := head
	for _, value := range values[1:] {
		tail.next = &Node{value, nil}
		tail = tail.next
	}
	if l.len == 0 {
		*l = List{newCount, head, tail}
	} else {
		l.len += newCount
		l.tail.next = head
	}
}

func (l *List) Value(index int) interface{} {
	node := l.find(index)
	v := node.value
	return v
	// return l.find(index).value
}

func (l *List) Index(value interface{}) int {
	for index, node := 0, l.head; node != nil; node = node.next {
		if node.value == value {
			return index
		}
		index++
	}
	return -1
}

func (l *List) Insert(atIndex int, value interface{}) {
	l.checkIndex(atIndex)
	if atIndex == 0 {
		old := l.head
		l.head = &Node{value, old}
	} else {
		previous := l.find(atIndex - 1)
		old := previous.next
		previous.next = &Node{value, old}
	}
	l.len++

}

func (l *List) Delete(index int) {
	l.checkIndex(index)
	if index == 0 {
		l.head = l.head.next
		if l.len <= 2 {
			l.tail = l.head
		}
	} else {
		previous := l.find(index - 1)
		previous.next = previous.next.next
		if l.len-1 == index {
			l.tail = previous
		}
	}
	l.len--
}

func (l *List) Clear() {
	*l = List{}
}

func (l *List) Clone() *List {
	cp := *l
	if l.head == nil {
		return &cp
	}
	head := l.head.Clone()
	node := head
	for node.next != nil {
		node.next = node.next.Clone()
		node = node.next
	}
	cp.head, cp.tail = head, node
	return &cp

}

func (l *List) Reverse() {
	var last, node, next *Node = nil, l.head, nil
	for node != nil {
		next, node.next = node.next, last
		last, node = node, next
	}
	l.head, l.tail = l.tail, l.head
}
