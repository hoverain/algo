package singlylinkedlist

import (
	"fmt"
)

type Node struct {
	value interface{}
	next  *Node
}

func (n Node) String() string {
	return fmt.Sprint(n.value)
}

func (n Node) Next() *Node {
	return n.next
}

func (n Node) Len() int {
	l := 1
	for node := n.next; node != nil; node = node.next {
		l++
	}
	return l
}

func (n Node) Value() interface{} {
	return n.value
}

func (n *Node) Clone() *Node {
	return &Node{n.value, n.next}
}

func NewNode(value interface{}, next *Node) *Node {
	return &Node{value, next}
}
