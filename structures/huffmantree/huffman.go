package huffmantree

import (
	"fmt"
	// "strconv"
)

type VW struct {
	Value  interface{}
	Weight int
}

type Node struct {
	VW
	Parent int
	LChild int
	RChild int
}

type Code struct {
	Bits   uint32
	Length int8
}

// func (c Code) String() string {
// 	return fmt.Sprintf("%0"+strconv.Itoa(int(c.Length))+"b", c.Bits)
// }

func (c Code) String() string {
	offset := byte('1' - 1)
	maxLength := uint32(c.Length)
	var bytes = make([]byte, c.Length)
	for l := maxLength; l > 0; l-- {
		bytes[maxLength-l] = byte(c.Bits<<(32-l)>>31) + offset
	}
	return string(bytes)
}

func NewTree(vws []VW) []Node {
	length := len(vws)
	nodes := make([]Node, 0, 2*length-1)
	for i := 0; i < length; i++ {
		nodes = append(nodes, Node{vws[i], -1, -1, -1})
	}
	maxInt := int(^uint(0) >> 1)
	for i := 0; i < length-1; i++ {
		var i1, i2 int
		m1, m2 := maxInt, maxInt
		for j := 0; j < i+length; j++ {
			if nodes[j].Weight < m1 && nodes[j].Parent == -1 {
				i1, i2 = j, i1
				m1, m2 = nodes[j].Weight, m1
			} else if nodes[j].Weight < m2 && nodes[j].Parent == -1 {
				i2 = j
				m2 = nodes[j].Weight
			}
		}
		nodes[i1].Parent = length + i
		nodes[i2].Parent = length + i
		nodes = append(nodes, Node{VW{nil, m1 + m2}, -1, i1, i2})
	}
	return nodes
}

func CodesFromVWs(vws []VW) []Code {
	nodes := NewTree(vws)
	return CodesFromTree(nodes)
}

func CodesFromTree(tree []Node) []Code {
	parentIndex := 0
	for tree[parentIndex].Parent != -1 {
		parentIndex = tree[parentIndex].Parent
	}
	stack := []int{parentIndex}
	codes := make([]Code, len(tree))
	for len(stack) != 0 {
		index := stack[0]
		node := tree[index]
		stack = stack[1:]
		if node.LChild != -1 {
			stack = append(stack, node.LChild)
			codes[node.LChild].Length = codes[index].Length + 1
			codes[node.LChild].Bits = codes[index].Bits << 1
		}
		if node.RChild != -1 {
			stack = append(stack, node.RChild)
			codes[node.RChild].Length += codes[index].Length + 1
			codes[node.RChild].Bits = codes[index].Bits<<1 + 1
		}
		fmt.Println(codes)
	}
	return codes[:(len(tree)+1)/2]
}

func Debug() {
	fmt.Printf("%02b", 1)
}
