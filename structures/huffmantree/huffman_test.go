package huffmantree_test

import (
	"fmt"
	"hover/algo/structures/huffmantree"
	"testing"
)

type testdata struct {
	inputs []huffmantree.VW
	codes  []string
}

var testGroups = []testdata{
	{
		[]huffmantree.VW{{"a", 5}, {"b", 32}, {"c", 18}, {"d", 7}, {"e", 25}, {"f", 13}},
		[]string{"1000", "11", "00", "1001", "01", "101"},
	},
}

func TestHuffman(t *testing.T) {
	for _, group := range testGroups {
		nodes := huffmantree.NewTree(group.inputs)
		for i := 0; i < len(nodes); i++ {
			t.Log("node: ", nodes[i])
		}
		codes := huffmantree.CodesFromTree(nodes)
		t.Log("codes: ", codes)
		got := fmt.Sprint(codes)
		expected := fmt.Sprint(group.codes)
		if got != expected {
			t.Errorf("expected(%v), got(%v)", expected, got)
		}
	}
}
