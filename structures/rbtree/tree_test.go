package rbtree

import (
	"fmt"
	"log"
	"math/rand"
	"testing"
)

type Num int

func (n Num) Less(other Value) bool {
	if v := other.(Num); n < v {
		return true
	}
	return false
}

func (n *Num) String() string {
	return "*" + fmt.Sprint(*n)
}

func valuesFromInts(ints []int) []Value {
	values := make([]Value, 0, len(ints))
	for _, v := range ints {
		values = append(values, Num(v))
	}
	return values
}

func TestDebug(t *testing.T) {
	seq := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	rand.Shuffle(10, func(i, j int) {
		seq[i], seq[j] = seq[j], seq[i]
	})
	t.Log(seq)
	values := valuesFromInts(seq)
	tree := NewTree(values)
	log.Println(tree)
	if !Check(tree) {
		log.Fatal("invalid tree")
	}
}
