package rbtree

import (
	"fmt"
	"strings"
)

// "log"
// "os"

// var logger log.Logger = *log.New(os.Stderr, "RedBlackTree ", log.LstdFlags)

type Value interface {
	Less(other Value) bool
}

type Node struct {
	Value    Value
	IsRChild bool
	Black    bool
	Parent   *Node
	LChild   *Node
	RChild   *Node
}

func NewTree(values []Value) (tree *Node) {
	for _, v := range values {
		tree = tree.Append(v)
		fmt.Println("-----\n", tree)
	}
	return
}

func (tree *Node) String() string {
	b := new(strings.Builder)
	tree.Traverse(func(node *Node) bool {
		if node == nil {
			fmt.Fprintln(b, nil)
		} else {
			fmt.Fprintf(b, "%p => %#v\n", node, *node)
		}
		return true
	})
	return b.String()
}

func (root *Node) Append(v Value) (newRoot *Node) {
	newNode := &Node{v, false, false, nil, nil, nil}
	if root == nil {
		newNode.Black = true
		return newNode
	}
	parent := root
	for {
		if v.Less(parent.Value) {
			if parent.LChild == nil {
				parent.LChild = newNode
				newNode.IsRChild = false
				newNode.Parent = parent
				return adjust(newNode)
			}
			parent = parent.LChild
		} else {
			if parent.RChild == nil {
				parent.RChild = newNode
				newNode.IsRChild = true
				newNode.Parent = parent
				return adjust(newNode)
			}
			parent = parent.RChild
		}
	}
}

func (node *Node) Root() (root *Node) {
	root = node
	for root.Parent != nil {
		root = root.Parent
	}
	return root
}

func adjust(node *Node) (newRoot *Node) {
	parent := node.Parent
	if parent == nil {
		return node
	}
	if parent.Parent == nil {
		return parent
	}
	if node.Black || parent.Black {
		return node.Root()
	}
	aunt := parent.Parent.LChild
	if !parent.IsRChild {
		aunt = parent.Parent.RChild
	}
	if aunt == nil || aunt.Black {
		newParent := rotate(node)
		return adjust(newParent)
	} else {
		colorFlip(parent.Parent)
		return adjust(parent.Parent)
	}
}

// if parent.Parent.RChild.Black {
// 	newGrandParent := llRotate(node)
// 	return adjust(newGrandParent)
// } else {
// 	colorFlip(parent.Parent)
// 	return adjust(parent.Parent)
// }

// colorFlip make the parent red and it's children black
func colorFlip(parent *Node) {
	parent.Black = false
	if parent.LChild != nil {
		parent.LChild.Black = true
	}
	if parent.RChild != nil {
		parent.RChild.Black = true
	}
	if parent.Parent == nil {
		parent.Black = true
	}
}

func rotate(node *Node) (newParent *Node) {
	parent := node.Parent
	grandParent := parent.Parent
	IsGrandParentRchild := grandParent.IsRChild
	grandGrandParent := grandParent.Parent
	if !node.Parent.IsRChild {
		if !node.IsRChild {
			newParent = llRotate(grandParent)
		} else {
			newParent = lrRotate(grandParent)
		}
	} else {
		if !node.IsRChild {
			newParent = rlRotate(grandParent)
		} else {
			newParent = rrRotate(grandParent)
		}
	}
	if grandGrandParent != nil {
		newParent.Parent = grandGrandParent
		if !IsGrandParentRchild {
			grandGrandParent.LChild = newParent
		} else {
			grandGrandParent.RChild = newParent
		}
	}
	afterRotate(newParent)
	return
}

func llRotate(node *Node) (newParent *Node) {
	child := node.LChild
	node.LChild = child.RChild
	if node.LChild != nil {
		node.LChild.Parent = node
		node.LChild.IsRChild = false
	}
	child.RChild = node
	child.Parent = node.Parent
	child.IsRChild = node.IsRChild
	node.Parent = child
	node.IsRChild = true
	return child
}

func rrRotate(node *Node) (newParent *Node) {
	child := node.RChild
	node.RChild = child.LChild
	if node.RChild != nil {
		node.RChild.Parent = node
		node.RChild.IsRChild = true
	}
	child.LChild = node
	child.Parent = node.Parent
	child.IsRChild = node.IsRChild
	node.Parent = child
	node.IsRChild = false
	return child
}

func lrRotate(node *Node) (newParent *Node) {
	node.LChild = rrRotate(node.LChild)
	return llRotate(node)
}

func rlRotate(node *Node) (newParent *Node) {
	node.RChild = llRotate(node.RChild)
	return rrRotate(node)
}

// afterRotate we make the parent black and it's children red
func afterRotate(parent *Node) {
	parent.Black = true
	if parent.LChild != nil {
		parent.LChild.Black = false
	}
	if parent.RChild != nil {
		parent.RChild.Black = false
	}
}

// Check tells if the tree is a red-black-tree or not
func Check(root *Node) (isRedBlackTree bool) {
	if root == nil {
		return false
	}
	isRedBlackTree = true
	blackNodeCountTillLeafShouldBe := 0
	blackNodeCountTillLeafMap := make(map[*Node]int)
	root.Traverse(func(node *Node) bool {
		blackNodeCount := 0
		if node.Parent != nil {
			if !node.Parent.Black && !node.Black {
				isRedBlackTree = false
				return false
			}
			blackNodeCount = blackNodeCountTillLeafMap[node.Parent]
			if node.Black {
				blackNodeCount++
			}
			blackNodeCountTillLeafMap[node] = blackNodeCount
		}
		if node.LChild == nil || node.RChild == nil {
			// leaf
			if blackNodeCountTillLeafShouldBe == 0 {
				blackNodeCountTillLeafShouldBe = blackNodeCount + 1
			} else if blackNodeCount+1 != blackNodeCountTillLeafShouldBe {
				isRedBlackTree = false
				return false
			}
		}
		return true
	})
	return
}

func (tree *Node) Traverse(fn func(node *Node) bool) {
	if tree == nil {
		return
	}
	if fn(tree) {
		tree.LChild.Traverse(fn)
		tree.RChild.Traverse(fn)
	}
}
