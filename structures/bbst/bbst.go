package bbst

import (
	"fmt"
	"strconv"
	"strings"
)

type Node struct {
	Index int
	Data  interface{}
}

type Tree struct {
	Node
	height int
	lchild *Tree
	rchild *Tree
}

func NewTree(nodes []Node) (root *Tree) {
	for i := range nodes {
		root = root.Insert(nodes[i])
	}
	return root
}

func (t Tree) String() string {
	var b = new(strings.Builder)
	b.WriteString("bbst:\n")
	stack := []Tree{t}
	for len(stack) != 0 {
		t, stack = stack[0], stack[1:]
		b.WriteString(fmt.Sprintf("{%v, %v, %v, ", t.Index, t.Data, t.height))
		if t.lchild != nil {
			stack = append(stack, *t.lchild)
			b.WriteString(strconv.Itoa(t.lchild.Index))
		} else {
			b.WriteString("<nil>")
		}
		b.WriteString(", ")
		if t.rchild != nil {
			stack = append(stack, *t.rchild)
			b.WriteString(strconv.Itoa(t.rchild.Index))
		} else {
			b.WriteString("<nil>")
		}
		b.WriteString("}\n")
	}
	b.WriteByte('\n')
	return b.String()
}

func (t *Tree) Height() int {
	if t == nil {
		return -1
	} else {
		return t.height
	}
}

func (t *Tree) Find(index int) (data interface{}, found bool) {
	tree := t.FindTree(index)
	if tree == nil {
		return
	}
	return tree.Data, true
}

func (t *Tree) FindTree(index int) *Tree {
	if t == nil {
		return nil
	} else if index == t.Index {
		return t
	} else if index < t.Index {
		return t.lchild.FindTree(index)
	} else {
		return t.rchild.FindTree(index)
	}
}

func (t *Tree) Insert(node Node) *Tree {
	if t == nil {
		return &Tree{node, 0, nil, nil}
	}
	if node.Index == t.Index {
		return t
	}
	if node.Index < t.Index {
		t.lchild = t.lchild.Insert(node)
		if t.lchild.Height()-t.rchild.Height() > 1 {
			if node.Index < t.lchild.Index {
				t = t.llRotate()
			} else {
				t = t.lrRotate()
			}
		}
	} else {
		t.rchild = t.rchild.Insert(node)
		if t.rchild.Height()-t.lchild.Height() > 1 {
			if node.Index > t.rchild.Index {
				t = t.rrRotate()
			} else {
				t = t.rlRotate()
			}
		}
	}
	t.updateHeight()
	return t
}

func (t *Tree) Delete(index int) *Tree {
	if t == nil {
		return t
	}
	if index == t.Index {
		if t.rchild == nil {
			return t.lchild
		}
		next := t.rchild
		for next.lchild != nil {
			next = next.lchild
		}
		t.Node = next.Node
		t.rchild = t.rchild.Delete(index)
		t.updateHeight()
		return t
	}
	if index < t.Index {
		t.lchild = t.lchild.Delete(index)
	} else {
		t.rchild = t.rchild.Delete(index)
	}
	t.updateHeight()
	if t.lchild != nil {
		t.lchild = t.lchild.adjust()
	}
	if t.rchild != nil {
		t.rchild = t.rchild.adjust()
	}
	return t.adjust()
}

func (t *Tree) updateHeight() {
	lchildHeight, rchildHeight := t.lchild.Height(), t.rchild.Height()
	if lchildHeight > rchildHeight {
		t.height = lchildHeight + 1
	} else {
		t.height = rchildHeight + 1
	}
}

func (t *Tree) llRotate() *Tree {
	root := t.lchild
	t.lchild = t.lchild.rchild
	root.rchild = t
	t.updateHeight()
	root.updateHeight()
	return root
}

func (t *Tree) rrRotate() *Tree {
	root := t.rchild
	t.rchild = t.rchild.lchild
	root.lchild = t
	t.updateHeight()
	root.updateHeight()
	return root
}

func (t *Tree) lrRotate() *Tree {
	t.lchild = t.lchild.rrRotate()
	return t.llRotate()
}

func (t *Tree) rlRotate() *Tree {
	t.rchild = t.rchild.llRotate()
	return t.rrRotate()
}

func (t *Tree) adjust() *Tree {
	if t == nil {
		return t
	}
	if t.lchild.Height()-t.rchild.Height() > 1 {
		if t.lchild.lchild.Height() >= t.lchild.rchild.Height() {
			t = t.llRotate()
		} else {
			t = t.lrRotate()
		}
	}
	if t.rchild.Height()-t.lchild.Height() > 1 {
		if t.rchild.rchild.Height() >= t.rchild.lchild.Height() {
			t = t.rrRotate()
		} else {
			t = t.rlRotate()
		}
	}
	t.updateHeight()
	return t
}
