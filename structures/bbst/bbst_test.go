package bbst_test

import (
	"hover/algo/structures/bbst"
	"testing"
)

func makeNodes(indices []int) []bbst.Node {
	nodes := make([]bbst.Node, len(indices))
	for i, index := range indices {
		nodes[i] = bbst.Node{index, index}
	}
	return nodes
}

func TestDebug(t *testing.T) {
	// nodes := []bbst.Node{
	// 	{24, 24},
	// 	{69, 69},
	// 	{19, 19},
	// 	{25, 25},
	// 	{5, 5},
	// 	{16, 16},
	// 	{75, 75},
	// 	{90, 90},
	// 	{32, 32},
	// }
	nodes := makeNodes([]int{65, 56, 80, 25, 60, 75, 90, 58, 70, 78, 98, 68})
	tree := bbst.NewTree(nodes)
	t.Log(tree)
	t.Log("\n")
	t.Log(tree.Delete(80))
}
