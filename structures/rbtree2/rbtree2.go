package rbtree2

type Value interface {
	LessThan(Value) bool
	Equal(Value) bool
}

type Node struct {
	Value
	red    bool
	parent *Node
	lchild *Node
	rchild *Node
}

type Tree struct {
	Root *Node
}

func (t *Tree) Push(v Value) {

}

func (t *Tree) Remove(v Value) {

}
