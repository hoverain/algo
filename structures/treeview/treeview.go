package treeview

import (
	"bufio"
	"bytes"
	"strings"
)

type Node interface {
	Value() string
	Children() []Node
}

type lines struct {
	data       [][]byte
	maxLineLen int
	pivot      int
}

func (l lines) Len() int {
	return len(l.data)
}

func (l lines) Pivot() int {
	if l.pivot == 0 && l.maxLineLen > 0 {
		return (l.maxLineLen - 1) / 2
	}
	return l.pivot
}

func pad(bytes []byte, paddingCount int) []byte {
	for i := 0; i < paddingCount; i++ {
		bytes = append(bytes, ' ')
	}
	return bytes
}

func StringToLines(str string) lines {
	ls := lines{make([][]byte, 0, 1), 0, 0}
	scanner := bufio.NewScanner(strings.NewReader(str))
	for scanner.Scan() {
		newLine := scanner.Bytes()
		ls.data = append(ls.data, newLine)
		if len(newLine) > ls.maxLineLen {
			ls.maxLineLen = len(newLine)
		}
	}
	return ls
}

func LinesToBytes(ls lines) []byte {
	var buf bytes.Buffer
	for _, line := range ls.data {
		buf.Write(line)
		buf.WriteByte('\n')
	}
	return buf.Bytes()
}

func LinesToString(ls lines) string {
	var builder strings.Builder
	for _, line := range ls.data {
		builder.Write(line)
		builder.WriteByte('\n')
	}
	return builder.String()
}

func stackLinesGroups(groups []lines) lines {
	newData := make([][]byte, 0)
	var newMaxLineLen int
	pivot := 0
	for _, ls := range groups {
		for _, line := range ls.data {
			newData = append(newData, line)
			if len(line) > newMaxLineLen {
				newMaxLineLen = len(line)
			}
		}
		if pivot == 0 && ls.Pivot() > 0 {
			pivot = ls.Pivot()
		}
	}
	return lines{newData, newMaxLineLen, pivot}
}

func combineLinesGroups(groups []lines, gap int) lines {
	if len(groups) == 0 {
		return lines{}
	}
	maxRow := 0
	for _, ls := range groups {
		if ls.Len() > maxRow {
			maxRow = ls.Len()
		}
	}
	if maxRow == 0 {
		return lines{}
	}
	newData := make([][]byte, maxRow)
	for n, ls := range groups {
		for i := 0; i < maxRow; i++ {
			if n > 0 {
				newData[i] = pad(newData[i], gap)
			}
			line := []byte{}
			if i < len(ls.data) {
				line = ls.data[i]
			}
			newData[i] = append(newData[i], line...)
			paddingCount := ls.maxLineLen - len(line)
			newData[i] = pad(newData[i], paddingCount)
		}
	}
	return lines{newData, len(newData[0]), 0}
}

func makeRoof(groups []lines, gap int) lines {
	if len(groups) == 0 {
		return lines{}
	}
	level1 := make([]byte, 0, groups[0].Len())
	var firstBranchIndex, lastBranchIndex int
	for n, ls := range groups {
		if n > 0 {
			level1 = pad(level1, gap)
		}
		lastBranchIndex = ls.Pivot() + len(level1)
		level1 = pad(level1, ls.maxLineLen)
		if n == 0 {
			firstBranchIndex = lastBranchIndex
		}
		level1[lastBranchIndex] = '|'
	}
	level2 := make([]byte, len(level1))
	for i := range level2 {
		if i > firstBranchIndex && i < lastBranchIndex {
			level2[i] = '_'
		} else {
			level2[i] = ' '
		}
	}
	// level2[(len(level2)-1)/2] = '|'
	pivot := (lastBranchIndex-firstBranchIndex)/2 + firstBranchIndex
	level2[pivot] = '|'
	return lines{[][]byte{level2, level1}, len(level1), pivot}
}

func middleArrange(ls lines) lines {
	newData := make([][]byte, ls.Len())
	for i, line := range ls.data {
		paddingCount := ls.maxLineLen - len(line)
		prefixPaddingCount := paddingCount / 2
		suffixPaddingCount := paddingCount - prefixPaddingCount
		newData[i] = pad(newData[i], prefixPaddingCount)
		newData[i] = append(newData[i], line...)
		newData[i] = pad(newData[i], suffixPaddingCount)
	}
	return lines{newData, ls.maxLineLen, ls.pivot}
}

func adjustParentPosition(parent lines, roofAndOthers lines) lines {
	if parent.maxLineLen > roofAndOthers.maxLineLen {
		return parent
	}
	roofLevel1 := roofAndOthers.data[1]
	roofTopMiddle := roofAndOthers.Pivot()
	roofLeftMostBranch := bytes.IndexByte(roofLevel1, '|')
	if roofLeftMostBranch == roofTopMiddle {
		roofLeftMostBranch = 0
	}
	parentStartIndex := roofTopMiddle - (parent.maxLineLen+1)/2
	if roofLeftMostBranch > parentStartIndex {
		parentStartIndex = parentStartIndex - (roofLeftMostBranch - parentStartIndex)
	}
	if parentStartIndex < 0 {
		parentStartIndex = 0
	}
	prefixPaddingCount := parentStartIndex + 1
	suffixPaddingCount := roofAndOthers.maxLineLen - parent.maxLineLen - prefixPaddingCount
	newData := make([][]byte, parent.Len())
	for i := range parent.data {
		newData[i] = pad(newData[i], prefixPaddingCount)
		newData[i] = append(newData[i], parent.data[i]...)
		newData[i] = pad(newData[i], suffixPaddingCount)
	}
	return lines{newData, roofAndOthers.maxLineLen, roofAndOthers.pivot}
}

// func adjustParentPosition(parent lines, roofAndOthers lines) lines {
// 	if parent.maxLineLen > roofAndOthers.maxLineLen {
// 		return parent
// 	}
// 	roofLevel2, roofLevel1 := roofAndOthers.data[0], roofAndOthers.data[1]
// 	roofTopMiddle := bytes.IndexByte(roofLevel2, '|')
// 	roofLeftMostBranch := bytes.IndexByte(roofLevel1, '|')
// 	if roofLeftMostBranch == roofTopMiddle {
// 		roofLeftMostBranch = 0
// 	}
// 	parentStartIndex := roofTopMiddle - (parent.maxLineLen+1)/2
// 	if roofLeftMostBranch > parentStartIndex {
// 		parentStartIndex = parentStartIndex - (roofLeftMostBranch - parentStartIndex)
// 	}
// 	if parentStartIndex < 0 {
// 		parentStartIndex = 0
// 	}
// 	prefixPaddingCount := parentStartIndex + 1
// 	suffixPaddingCount := roofAndOthers.maxLineLen - parent.maxLineLen - prefixPaddingCount
// 	newData := make([][]byte, parent.Len())
// 	for i := range parent.data {
// 		newData[i] = pad(newData[i], prefixPaddingCount)
// 		newData[i] = append(newData[i], parent.data[i]...)
// 		newData[i] = pad(newData[i], suffixPaddingCount)
// 	}
// 	return lines{newData, roofAndOthers.maxLineLen, roofAndOthers.pivot}
// }

func GetLinesView(n Node, gap int) lines {
	currentLines := StringToLines(n.Value())
	children := n.Children()
	if len(children) == 0 {
		return currentLines
	}
	childrenLinesGroups := make([]lines, len(children))
	for i := range children {
		childrenLinesGroups[i] = GetLinesView(children[i], gap)
	}
	roof := makeRoof(childrenLinesGroups, gap)
	childrenLines := combineLinesGroups(childrenLinesGroups, gap)
	roofAndChildren := stackLinesGroups([]lines{roof, childrenLines})
	parent := adjustParentPosition(currentLines, roofAndChildren)
	return middleArrange(stackLinesGroups([]lines{parent, roofAndChildren}))
}

func GetStringView(n Node, gap int) string {
	return LinesToString(GetLinesView(n, gap))
}
