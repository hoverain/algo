package treeview

import "testing"

type node struct {
	value    string
	children []*node
}

func (n *node) Value() string {
	return n.value
}

func (n *node) Children() []Node {
	nodes := make([]Node, len(n.children))
	for i := range n.children {
		nodes[i] = n.children[i]
	}
	return nodes
}

func TestDebug(t *testing.T) {
	samples := []string{
		"\n\n\naaaaaaaaaaaaaaaaa",
		"ab\ncd\nefgh",
		"ab",
	}
	t.Log(middleArrange(StringToLines(samples[1])))
	groups := make([]lines, 0)
	for _, sample := range samples {
		groups = append(groups, StringToLines(sample))
	}
	t.Logf("\n%s", LinesToString(makeRoof(groups, 4)))
	t.Logf("\n%s", LinesToString(combineLinesGroups(groups, 4)))
}

func TestPrint(t *testing.T) {
	a := &node{"aaaaaaaaaaaaaaaa", nil}
	b := &node{"b", nil}
	c := &node{"cccccccccccccccccccccccccccccccccccccccccccc", nil}
	d := &node{"d", nil}
	e := &node{"eeeeeeeeeeeeeeeeeeeeeeeeeee", []*node{a, b, c}}
	f := &node{"ffffffffffffffffffffffffffffffffffffffffffffffffffff", []*node{c, d}}
	g := &node{"g", []*node{e, f, c}}
	t.Logf("\n%s", GetStringView(g, 3))
}
